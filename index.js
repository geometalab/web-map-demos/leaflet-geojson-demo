var zoom = 10;
var initialCoords = [47.41330, 8.65639];

var map = L.map('map').setView(initialCoords, zoom);

map.attributionControl.setPrefix(false);

L.tileLayer('http://tile.osm.ch/osm-swiss-style/{z}/{x}/{y}.png', {
    attribution: '<a id="problem-attribution" ' +
        `href="http://www.openstreetmap.org/fixthemap?lat=${initialCoords[0]}&lon=${initialCoords[1]}&zoom=${zoom}">Report a problem</a> | ` +
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

var tableTennisLayer = new L.geoJSON().addTo(map);

fetch("https://api.npoint.io/113395de1b61b4bac50d")
    .then(response => response.json())
    .then(geojson => tableTennisLayer.addData(geojson));

map.on('move', function () {
    document.getElementById('problem-attribution').setAttribute('href', createProblemLink());
});

function createProblemLink() {
    const coords = map.getCenter();
    const zoom = map.getZoom();

    return `http://www.openstreetmap.org/fixthemap?lat=${coords.lat}&lon=${coords.lng}&zoom=${zoom}`;
}